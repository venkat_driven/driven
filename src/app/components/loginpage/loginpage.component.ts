import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.scss']
})
export class LoginpageComponent implements OnInit {

  loginForm: FormGroup;

/*   profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  }); */

  constructor(
    public fb: FormBuilder,
    public lgnService: LoginService
    ) {

  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      userName: [''],
      password: ['']
    });
  }

  onLogin() {
    this.lgnService.doLogin(this.loginForm.value)
    .subscribe(
      (sucess) => {

      },
      (error) => {

      }
    );

  }

}
