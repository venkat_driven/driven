import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public http: HttpClient) {

  }

  doLogin(loginData) {
    return this.http.post('/login', loginData);
  }
}
